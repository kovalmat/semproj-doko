// Copyright (c) 2018 Emiliana (twitter.com/Emiliana_vt)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

//This script was created entirely by Matyas Koval 2021, but because of inherent
//inspiration by the original project, the original copyright was also included


//script for drawing straight lines 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class DDPLineMovement : MonoBehaviour
{
    public Camera cam;
    public GameObject controller;
    public Vector3 endPos;

    public DokoDemoPainterPen LinePen;

    private Vector3 startLine;
    private Vector3 endLine;

    private bool down = false;
    private bool up = false;

    void Start()
    {
        gameObject.GetComponent<LineRenderer>().enabled = false; //line renderer for visualisation
    }

    void FixedUpdate()
    {
        
    }

    void Update()
    {
        if (SteamVR_Actions._default.GrabGrip[SteamVR_Input_Sources.RightHand].stateDown && startLine == Vector3.zero)
        {
            if (!gameObject.GetComponent<LineRenderer>().enabled)
            {
                gameObject.GetComponent<LineRenderer>().enabled = true;
            }
            startLine = endPos;
            cam.transform.position = endPos;
            gameObject.GetComponent<LineRenderer>().SetPosition(0, startLine);
            down = true;

        }
        else if (SteamVR_Actions._default.GrabGrip[SteamVR_Input_Sources.RightHand].stateUp && endLine == Vector3.zero)
        {
            endLine = endPos;
            up = true;
        }
        //visualisation of the renderer
        if (down)
        {
            gameObject.GetComponent<LineRenderer>().SetPosition(1, endPos);
        }
        //coroutine for drawing straight lines call
        if (endLine != Vector3.zero && startLine != Vector3.zero && down && up)
        {

            gameObject.GetComponent<LineRenderer>().SetPosition(0, Vector3.zero);
            gameObject.GetComponent<LineRenderer>().SetPosition(1, Vector3.zero);
            down = false;
            up = false;
            Debug.Log("start "+startLine);
            Debug.Log("end " + endLine);
            StartCoroutine(LerpPosition(startLine, endLine, Vector3.Distance(startLine, endLine)));
        }
    }
    //coroutine for drawing straight lines
    IEnumerator LerpPosition(Vector2 startPosition, Vector2 targetPosition, float duration)
    {
        float time = 0;
        Debug.Log("coroutine " );
        while (time < duration)
        {
            cam.transform.position = (Vector3.Lerp(startPosition, targetPosition, time / duration));
            time += Time.deltaTime;
            LinePen.penDown = true;
            yield return null;
        }
        LinePen.penDown = false;
        endLine = Vector3.zero;
        startLine = Vector3.zero;
        yield return null;
    }
}

