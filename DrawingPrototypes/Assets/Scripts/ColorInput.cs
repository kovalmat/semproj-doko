using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//script for visualiying the pen colour on a material  made by Matyas Koval 2021
public class ColorInput : MonoBehaviour
{
    public Material mat;
    private DokoDemoPainterPen painter;
    // Start is called before the first frame update
    void Start()
    {
        painter = GameObject.FindObjectOfType<DokoDemoPainterPen>();
    }

    // Update is called once per frame
    void Update()
    {
        mat.color = painter.color;

    }
}
