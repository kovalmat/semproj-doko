using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//script for visualising the target of the painting made by Matyas Koval 2021
public class PhysicsPointer : MonoBehaviour
{
    public float defaultLength = 3.0f;
    public DDPLineMovement painter;
    public Vector3 endPos;

    private LineRenderer lineRenderer = null;

    public Vector3 GetPos()
    {
        return endPos;
    }
    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    void Update()
    {
        UpdateLength();
    }

    private void UpdateLength()
    {
        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, calculateEnd());
    }

    private Vector3 calculateEnd()
    {
        RaycastHit hit = CreateForwardRaycast();
        Vector3 endPosition = DefaultEnd(defaultLength);

        if (hit.collider)
            endPosition = hit.point;

        painter.endPos = endPosition;
        endPos = endPosition;
        return endPosition;
    }

    private RaycastHit CreateForwardRaycast()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.forward);

        Physics.Raycast(ray, out hit, defaultLength);
        return hit;
    }

    private Vector3 DefaultEnd(float length)
    {
        return transform.position + (transform.forward * defaultLength);
    }
}
