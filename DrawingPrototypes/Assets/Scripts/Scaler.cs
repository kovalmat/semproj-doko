using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

//script for scaling objects made by Matyas Koval 2021
public class Scaler : MonoBehaviour
{
    public Vector3 ScaleFactor = new Vector3(0.01f,0.01f, 0 );
    private bool up;
    private bool down;
    private float scale;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (SteamVR_Actions._default.ScaleUp[SteamVR_Input_Sources.RightHand].stateDown)
        {
            up = true;

        }
        else if (SteamVR_Actions._default.ScaleUp[SteamVR_Input_Sources.RightHand].stateUp)
        {
            up = false;
        }
        if (up)
        {
            gameObject.transform.localScale += ScaleFactor;
        }

        if (SteamVR_Actions._default.ScaleDown[SteamVR_Input_Sources.RightHand].stateDown)
        {
            down = true;

        }
        else if (SteamVR_Actions._default.ScaleDown[SteamVR_Input_Sources.RightHand].stateUp)
        {
            down = false;
        }
        if (down)
        {
            gameObject.transform.localScale -= ScaleFactor;
        }

        scale = 1 / gameObject.transform.localScale.x;
        gameObject.GetComponent<DokoDemoPainterPaintable>().radiusFactor = scale;
    }
}
