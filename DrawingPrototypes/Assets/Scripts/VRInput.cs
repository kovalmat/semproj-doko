using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Valve.VR;

public class VRInput : BaseInput
{
    public Camera eventCamera = null;

    public SteamVR_Input_Sources controller;
    public SteamVR_Action_Boolean action;

    protected override void Awake()
    {
        GetComponent<BaseInputModule>().inputOverride = this;
    }

    public override bool GetMouseButton(int button)
    {
        Debug.Log(action.GetState(controller));
        return SteamVR_Actions._default.GrabPinch[SteamVR_Input_Sources.RightHand].state;
    }

    public override bool GetMouseButtonDown(int button)
    {
        Debug.Log(action.GetStateDown(controller));
        return action.GetStateDown(controller);
    }

    public override bool GetMouseButtonUp(int button)
    {
        Debug.Log(action.GetStateUp(controller));
        return action.GetStateUp(controller);
    }

    public override Vector2 mousePosition
    {
        get
        {
            return new Vector2(eventCamera.pixelWidth /2, eventCamera.pixelHeight / 2);
        }
    }
}
